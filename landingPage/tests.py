from django.test import TestCase, Client
from django.urls import resolve

from landingPage.views import landingPage, addStatus
from landingPage.models import Status

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class landingPageExistTestcase(TestCase):
    def test_landing_page_url_exist(self):
        respose = Client().get("/")
        self.assertEqual(respose.status_code, 200)

    def test_landing_page_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landingPage/landingPage.html')

    def test_landing_page_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, landingPage)

class landingPageDatabase(TestCase):
    def test_model_can_create_new_activity(self):
        #Creating a new activity
        new_activity = Status.objects.create(status="Halo Namaste")

        #Retrieving all available activity
        counting_all_available_activity = Status.objects.all().count()
        self.assertEqual(counting_all_available_activity,1)

    def test_can_save_a_POST_request(self):
        response = Client().post('/add/', data={'status' : 'Halo Namaste'})
        counting_all_available_activity = Status.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

        new_response = Client().get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Halo Namaste', html_response)

    def test_database_status_as_str(self):
        #Creating a new activity
        new_activity = Status.objects.create(status="Aduh Pusing Ah")

        self.assertEqual(str(new_activity),"Aduh Pusing Ah")

    def test_database_limit_300_char(self):
        string_status = ""

        for i in range(400):
            string_status += "a"

        response = Client().post('/add/', data={'status' : string_status})
        
        counting_all_available_activity = Status.objects.all().count()
        self.assertEqual(counting_all_available_activity,0)

class Lab5FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Lab5FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab5FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        # find the form element
        status = selenium.find_element_by_name('status')

        submit = selenium.find_element_by_name('submit')

        # Fill the form with data
        status.send_keys('Mengerjakan Lab PPW')

        # submitting the form
        submit.send_keys(Keys.RETURN)
