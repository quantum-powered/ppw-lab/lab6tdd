from django import forms
from .models import Status

class FormStatus(forms.ModelForm):
    class Meta():
        model = Status
        fields = ['status']
        widgets = {
            'status' : forms.TextInput(
                attrs = {
                    'class' : 'form-control',
                    'placeholder': 'Aku Lagi Pengen Mie Ayam Nih..',
                    'name' : 'status'
                },
            )
        }
        labels = {
            'status': "",
        }