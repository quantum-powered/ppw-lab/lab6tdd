from django.shortcuts import render, redirect
from .forms import FormStatus
from .models import Status

# Create your views here.
def landingPage(request):
    form = FormStatus()
    status_list = Status.objects.order_by('-timestamp')
    query = {'form' : form, 'status':status_list}
    return render(request, 'landingPage/landingPage.html', context=query)

def addStatus(request):
    if request.method == 'POST':
        form = FormStatus(request.POST)

        if form.is_valid():
            form.save(commit = True)

    return redirect('/')